import static org.junit.Assert.*;
import org.junit.Test;
import java.util.ResourceBundle;

public class TestProject {
    ResourceBundle res = ResourceBundle.getBundle("data");
    MClass example = new MClass(); 
    @Test 
    public void testOut(){ 
    	assertEquals(res.getString("working_string"), example.check()); 
    } 
    @Test 
    public void testMath(){ 
    	assertTrue(example.number(7)==49); 
    } 
    @Test 
    public void testNull(){ 
    	assertNotNull(new MClass()); 
    }
}
